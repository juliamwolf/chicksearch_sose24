using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fox : MonoBehaviour
{
    public float speed = 5.0f; //Bewegungsgeschwindigkeit des Fuchses
    public float gravity = -9.81f;

    public Transform groundChecker; //Position f�r CheckSphere
    public float groundDistance = 0.2f;
    public LayerMask groundLayer;
    private bool isGrounded = false;

    public float jumpHeight = 2f; //gew�nschte Sprungh�he in m

    private Vector3 velocity = Vector3.zero; // Startgeschwindigkeit: (0, 0, 0)

    private CharacterController myController; //CharacterController-Komponente

    private Transform camTransform;

    public int chickNumber = 0;
    public int maxChickNumber = 3;
    public HUDManager hudManager;

    private Animator animController;
    private bool gameWon = false;
    private TerrainChecker terrainChecker;

    private AudioSource yaySound;

    // Start is called before the first frame update
    void Start()
    {
        //Zugriff auf die CharacterController-Komponente erhalten
        myController = GetComponent<CharacterController>();
        camTransform = GameObject.FindWithTag("MainCamera").transform;

        maxChickNumber = GameObject.FindObjectsByType<Chick>(FindObjectsSortMode.None).Length;

        animController = GetComponent<Animator>(); //Zugriff auf die AnimatorController-Komponente erhalten
        terrainChecker = GetComponentInChildren<TerrainChecker>(); //Zugriff auf die Terrain-Check-Sphere erhalten

        yaySound = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        //Wenn Level schon geschafft...
        if (gameWon)
        {
            return; //... brich die Update-Funktion ab
        }

        //BEWEGUNG auf dem Boden (x- und z-Richtung)

        //Input auslesen
        float horizontalInput = Input.GetAxis("Horizontal");
        float verticalInput = Input.GetAxis("Vertical");

        //Vector f�r die Bewegung auf der Fl�che berechnen
        Vector3 move = new Vector3(horizontalInput, 0f, verticalInput);

        //Bewegung in Kamera-Richtung drehen
        move = Quaternion.Euler(0f, camTransform.rotation.eulerAngles.y, 0f) * move;

        //Bewegung in x- und z-Richtung anwenden:
        myController.Move(move * speed * Time.deltaTime);

        //Character in Bewegungsrichtung drehen:
        if(move != Vector3.zero)
        {
            transform.forward = move;
        }
        


        //SCHWERKRAFT

        //Gravitation auf y-Geschwindigkeit des Characters anwenden
        velocity.y = velocity.y + gravity * Time.deltaTime;

        //Steht der Character auf dem Boden?
        isGrounded = Physics.CheckSphere(groundChecker.position, groundDistance, groundLayer, QueryTriggerInteraction.Ignore);

        //Wenn der Character auf dem Boden ist und nicht gerade zum Sprung ansetzt...
        if(isGrounded == true && velocity.y < 0f)
        {
            velocity.y = 0f; //...setze die y-Geschwindigkeit auf 0 zur�ck.
        }


        //SPRUNG

        //Wenn der Jump-Button gedr�ckt wird und der Character auf dem Boden steht...
        if (Input.GetButtonDown("Jump") && isGrounded == true)
        {
            //... gib einen Sprung-Impuls nach oben.
            velocity.y = Mathf.Sqrt(gravity * jumpHeight * -2f); //Herleitung: https://medium.com/@brazmogu/physics-for-game-dev-a-platformer-physics-cheatsheet-f34b09064558
        }

        //Bewegung in y-Richtung anwenden:
        myController.Move(velocity * Time.deltaTime);


        //ANIMATOR CONTROLLER UPDATEN

        animController.SetBool("InAir", !isGrounded && !terrainChecker.isTouchingTerrain);
        animController.SetFloat("Speed", move.magnitude);
    }

    public void CollectChick()
    {
        chickNumber = chickNumber + 1; //Anzahl gesammelter K�ken hochz�hlen

        //Debug.Log("Chick #" + chickNumber + " eingesammelt!");

        if(chickNumber >= maxChickNumber) //Ist die Anzahl gesammelter K�ken (gr��er oder) gleich der Maximal-Anzahl?
        {
            //Debug.Log("SPIEL GEWONNEN!");
            hudManager.ShowWinPanel(); //Win-Panel einblenden
            Time.timeScale = 0f; //Spiel pausieren
            gameWon = true; //Flag setzen, um Update abzubrechen

            yaySound.Play(); //AudioClip abspielen
        }
    }
}
