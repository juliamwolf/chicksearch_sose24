using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    public AudioMixer masterMixer;

    public void LoadLevel(string levelName)
    {
        SceneManager.LoadScene(levelName); //Szene laden
        Time.timeScale = 1f; //Pausieren des Spiels aufheben
    }

    public void QuitGame()
    {
        Application.Quit(); //Spiel schließen (nur Standalone)

#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false; //Play-Modus im Editor beenden
#endif
    }

    //Lautstärke auf aktuellen Slider-Wert setzen
    public void SetMasterVolume(float sliderValue)
    {
        //masterMixer.SetFloat("MasterVolume", sliderValue); //Linearer Slider, Werte: -40 bis 8
        masterMixer.SetFloat("MasterVolume", Mathf.Log10(sliderValue) * 20f); //Logarithmischer Slider, Werte: 0.0001 bis 2
        
        //Herleitung: https://www.dr-lex.be/info-stuff/volumecontrols.html
    }

    public void SetSoundVolume(float sliderValue)
    {
        masterMixer.SetFloat("SoundFXVolume", sliderValue);
    }
}
