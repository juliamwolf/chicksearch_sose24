using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;

public class HUDManager : MonoBehaviour
{
    public TextMeshProUGUI scoreText;
    public GameObject winPanel;

    public Fox player;

    public EventSystem eventSystem;
    public GameObject mainMenuButton;

    // Start is called before the first frame update
    void Start()
    {
        winPanel.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        scoreText.text = player.chickNumber + "/" + player.maxChickNumber; //Anzahl K�ken auf dem UI ausgeben
    }

    public void ShowWinPanel()
    {
        winPanel.SetActive(true);
        eventSystem.SetSelectedGameObject(mainMenuButton);
    }
}
