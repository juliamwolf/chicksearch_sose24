using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class TestScript : MonoBehaviour
{
    public int zahl = 5;

    public float kommazahl = 1.0f;

    // Start is called before the first frame update
    void Start()
    {
        zahl = AddNumbers(8, 19);

        DisplayNumber();
    }

    // Update is called once per frame
    void Update()
    {
        DisplayNumber();
    }

    int AddNumbers(int klaus, int hannes)
    {
        return 2*(hannes + klaus);
    }

    void DisplayNumber()
    {
        Debug.Log("Meine Zahl: " + zahl);
    }
}

