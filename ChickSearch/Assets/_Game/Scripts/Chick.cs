using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chick : MonoBehaviour
{
    private Animator animController;

    public GameObject chickFXprefab;

    private void Awake()
    {
        animController = GetComponent<Animator>();

        animController.SetFloat("Offset", Random.Range(0f, 1f));
    }

    private void OnTriggerEnter(Collider other)
    {
        //Wenn das GameObject, das in den Trigger-Bereich eintritt, den Tag "Player" hat...
        if (other.CompareTag("Player"))
        {     
            other.gameObject.GetComponent<Fox>().CollectChick();

            Instantiate(chickFXprefab, transform.position, transform.rotation);

            Destroy(gameObject); //... zerst�re das GameObject, auf dem sich dieses Skript befindet (K�ken)
        }
        
    }

}
