using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainChecker : MonoBehaviour
{

    public bool isTouchingTerrain = false;

    private void OnTriggerEnter(Collider other)
    {
        isTouchingTerrain = true;
    }

    private void OnTriggerStay(Collider other)
    {
        isTouchingTerrain = true;
    }

    private void OnTriggerExit(Collider other)
    {
        isTouchingTerrain = false;
    }

}
