using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyAfterDelay : MonoBehaviour
{
    public float delay = 2f;

    // Start is called before the first frame update
    void Start()
    {
        Invoke("SelftDestruct", delay); //Rufe SelftDestruct() nach einer gewissen Zeit auf (delay von z.B. 2 Sek)
    }

    private void SelftDestruct()
    {
        Destroy(gameObject);
    }
}
